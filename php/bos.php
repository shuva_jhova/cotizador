<?php

	header('Content-Type: application/json');
	$dataPost = file_get_contents("php://input");
	
	$bos = new bos();
	$fncR = $bos->request($dataPost);
	
	try
	{
		die(json_encode($fncR,JSON_NUMERIC_CHECK));
	}
	catch (Exception $e){
		trigger_error(print_r($e,false));
	}
	class bos
	{
		function request($strData)
		{
			$c_libpath = '';
			$response = new response();
			//trigger_error("-".$strData."-");
			if(substr($strData,0,1)=='{' && substr($strData,-1)=='}')
			{
				try
				{
					$objData = json_decode($strData,true);
					
					$cls	= $objData['Class'];
					$fnc	= $objData['Function'];
					$params	= $objData['Params'];
					
					if(is_array($params) || is_null($params))
					{
						if(is_null($params)) $params = array();
						//$strClassPath = $c_libpath . '/' . $cls . '.php';
						$strClassPath =  $cls . '.php';

						if(file_exists($strClassPath))
						{
							require_once($strClassPath);
							try
							{
								$clsname = basename($cls);
								$clase = new $clsname();
								if(method_exists($clase,$fnc))
								{
									$resultFnc = call_user_func_array(array($clase,$fnc), $params);

									$response->Result = $resultFnc;
									$response->Success = true;
								}
								else
								{
									$response->Success = false;
									$response->Error = "La funci�n $fnc no existe en la clase $cls";
								}
								
							} catch(Exception $e)
							{
								$response->Success = false;
								$response->Error = "La funci�n $fnc gener� un error:\n".$e;
							}
						}
						else
						{
							$response->Success = false;
							$response->Error = "Clase $cls inexistente";
						}
					}
					else
					{
						$response->Success = false;
						$response->Error = "El atributo Params no es de tipo matriz";
					}
				}
				catch(Exception $e)
				{
					$response->Success = false;
					$response->Error = $e->getMessage();
				}
				
			}
			else
			{
				$response->Success = false;
				$response->Error = "El contenido no est� en formato JSON";
			}
			if($fnc=='register') trigger_error(print_r($response,false));
//trigger_error(print_r($response,true));		
	return $response;
		}
	}
	
	class response
	{
		public $Error = '';
		public $Success = false;
		public $Result = NULL;
	}
?>