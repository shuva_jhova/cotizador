﻿<?php
require_once("usersFunction.php");
$app = new usersFunction();
	if(!$app->islogged()){
		echo "<script>window.top.location.href = 'logout.php';</script>";	
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="initial-scale=1.0,user-scalable=yes"/>

<link rel="stylesheet" href="../css/estilo2.css">

 <script language="javascript" type="text/javascript" src="../js/gateway.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery-1.12.4.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
  <script language="javascript" type="text/javascript" src="../js/reporteinsumos.js"></script>

	<script language="javascript" src="../js/xlsx.full.min.js"></script>
	
	<script language="javascript" src="../js/FileSaver.js"></script>
	
	<script language="javascript" src="../js/tableexport.js"></script>

<title></title>

</head>
<body onload="getStatus();">
<main>
	
	<section id="titulo">
		<center><h2>Reporte de insumos por ordenes</h2>
		</center>
	</section>
<div>
	<form name="f1" action="#">
		<center>
		<div ">
			<div id="myDiv">
				<!--div class="txt">
					Ingresa el nombre del Insumo a buscar<br>
					<input type="text" name="num" placeholder="Nombre " id="txtbuscar" onkeyup="doSearch()" required="required" class="TT" />
				
				</div-->

				<div class="txt">
					Fecha Inicial<br>
					<input type="date"  id="txtfechainicial" required="required" class="TT" />	
				</div>
				<div class="txt">
					Fecha Final<br>
					<input type="date"  id="txtfechafinal" required="required" class="TT" />	
				</div>
				<div class="txt">
					Status<br>
					<select id="optionstatus" onChange=""></select>
				</div>

				
				<div class="txt">
						<input type="button" name="insertar" Value="Actualizar" onClick="getplaneacion();" id="ok"/>
				</div>
			<div >
				<img src="../img/load.gif" class="imgload" id="imgload">
			</div>
		</div>
		</center>
		<center id='prin' style="display:none;">
		<div class="txt">
			<input type="button" name="insertar" Value="Imprimir" onClick="" id="ok"/>
		</div>
	</center>
	</form>
</div>
<center>
</br>

	</br>
<div id="scro">
 <div id="tabla">
	
     <table class="tbl-qa" id="resultado">
		  <thead>
		
 		  </thead>
		
		  <tbody>				
             	 </tbody>
              </table>
 </div>
	</br></br>
		<div id="sin" style="display:none;">
			<div class='myDiv'><div class='txt'>SIN PLANEACIÓN PARA LOS DATOS SELECCIONADOS</div></div>
		</div>
 </div>
</center>
</div>


</main>

</body>
</html>
