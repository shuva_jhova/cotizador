<?php
require_once("dbControl.php");
class appControl
{ 
	var $app;
	var $path="C:/AppServ/www/PlanCtrol/documentos/";
	var $path2="C:/AppServ/www/PlanCtrol";
	public function __construct()
		{
			$this->app = new dbControl();
		}
			
	
	

	//general functions of system//


	public function getpaises(){
		$datos=array();
		$result=$this->app->getrecord("paises","*","where activo like '1'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];	

			$datos[] = array(
					'id'=>$vec['id'],
					'codigo'=>$vec['iso'],
					'pais'=>$vec['nombre']				
					);			
                }
		
		
	return $datos; 
	}

	public function getcategorias(){
		$datos=array();
		$result=$this->app->getrecord("categorias","*","where activo like '1'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];	

			$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre']				
					);			
                }
		
		
	return $datos;
	}
	
	public function getsubensambles(){
		$datos=array();
		$result=$this->app->getrecord("subensambles","*","where activo like '1'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];	

			$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre']				
					);			
                }
		
		
	return $datos;
	}

	public function gethorasturno(){
		$datos=array();
		$result=$this->app->getrecord("turno","*","where activo like '1'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];	

			$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],	
					'horas'=>$vec['horas']		
					);			
                }
		
		
	return $datos;
	}
	public function getpresentacionventa(){
		$datos=array();
		$result=$this->app->getrecord("presentacionventa","*","where activo like '1'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];	

			$datos[] = array(
					'id'=>$vec['id'],
					'presentacion'=>$vec['presentacion']
					);			
                }
		
		
	return $datos; 
	}
	
	public function deletepaths ($codigo){
		$datos=array();
		if($this->app->deleterecord("paths"," where codigoProducto like '$codigo'")>0)
		{
			$datos[]=array(
				'exito'=>'si'
				);			
		}
	return $datos;		
	}
	
///////////////////////////////////////////////////////////////////////////////functions of products////////////////////////////////////////////////////////////////////////////////////////
	

	public function validateSAE($codigo){
		$datos=array();
		$result=$this->app->getrecord("productos","*","where codigoProducto like '$codigo'");
		if(sizeof($result)>0)
    		{	
			$vec=$result[0];
			$datos[] = array(
					'id'=>$vec['id'],
					'valido'=>"Existe"
					);			
                }else{
			$datos[] = array(
					'id'=>0,
					'valido'=>"Si"
					);			
		
		}
		
	return $datos; 
		
	}


	public function saveproducto ($codigo,$descripcion,$nombre,$presentacion,$marca,$codigobarras,$codigobarrasempaque,$pais,$fraccionarancelaria,$exportacion,$barniz,$foto,$fotodetalle,$notasespeciales,$corrugado,$corrugadospallet,$piezascorrugado,$camaspallete,$anchocorrugado,$piezaspallet,$largocorrugado,$pesopresentacion,$altrocorrugado,$pesocorrugado,$anchopallet,$pesopallete,$largopallet,$anchoproducto,$alturapallet,$largoproducto,$corrugadosporcama,$altoproducto){
		$datos=array();
		$doc1='';$doc2='';$doc3='';$doc4='';$doc5='';$doc6='';$doc7='';$doc8='';
		$result=$this->app->getrecord("productos","*","where codigoProducto like '$codigo'");
				
		if (sizeof($result) > 0) {
			$datos[]=array(
				'exito'=>'ya',
				'folio'=>0
			);
		} 
		else 
		{
			
			$result2=$this->app->getrecord("presentacionventa","*","where presentacion like '$presentacion'");
			//trigger_error(sizeof($result2));
			if(sizeof($result2)<=0){
				$presentacion=ucfirst ($presentacion);
				$this->app->saverecord("presentacionventa","null,'$presentacion','1'");
			}			

			$result=$this->app->getrecord("paths","*","where codigoProducto like '$codigo'");
			if(sizeof($result)>0)
    			{	
				for($z=0;$z<sizeof($result);$z++){
					$vec=$result[$z];
					switch($vec['archivo']){

						case 'doc1':
							$doc1=$vec['ruta'];
						break;

						case 'doc2':
							$doc2=$vec['ruta'];
						break;

						case 'doc3':
							$doc3=$vec['ruta'];
						break;

						case 'doc4':
							$doc4=$vec['ruta'];
						break;

						case 'doc5':
							$doc5=$vec['ruta'];
						break;

						case 'doc6':
							$doc6=$vec['ruta'];
						break;

						case 'doc7':
							$doc7=$vec['ruta'];
						break;

						case 'doc8':
							$doc8=$vec['ruta'];
						break;

					}


				}
			}
			
			
			$id=$this->app->saverecord('productos',"null,'$nombre','$codigo','$descripcion','$presentacion','$marca','$codigobarras','$codigobarrasempaque','$foto','$fotodetalle','$doc1','$doc2','$doc3','$doc4','$doc5','$doc6','$doc7','$doc8','$corrugado',$piezascorrugado,$anchocorrugado,$largocorrugado,$altrocorrugado,$anchopallet,$largopallet,$alturapallet,$corrugadosporcama,$corrugadospallet,$camaspallete,$piezaspallet,$pesopresentacion,$pesocorrugado,$pesopallete,$anchoproducto,$largoproducto,$altoproducto,'$pais','$fraccionarancelaria','$exportacion','$notasespeciales','$barniz','1'");			
			if($id>0 ){
				$this->app->deleterecord('paths',"where codigoProducto like '$codigo'");
				$datos[]=array(
					'exito'=>'si',
					'folio'=>$id
				);	
			}else{
				$datos[]=array(
					'exito'=>'no',
					'folio'=>0
				);	
			}			
		}


		
	return $datos;


}
public function getallproducts(){
		$datos=array();
		$result=$this->app->getrecord("productos","*"," ORDER BY productos.nombreProducto ASC");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
		
			$datos[] = array(
				'id'=>$vec['id'],
				'nombre'=>$vec['nombreProducto'],
				'codigo'=>$vec['codigoProducto'],	
				'descripcion'=>$vec['descripcion'],
				'presentacion'=>$vec['presentacionVenta'],
				'marca'=>$vec['marcaProducto'],
				'codigobarrasproducto'=>$vec['codigoBarrasProducto'],
				'codigobarrasempaque'=>$vec['codigoBarrasEmpaque'],
				'foto'=>$vec['fotos'],
				'fotodetalle'=>$vec['fotosDetalle'],
				'doc1'=>$vec['documento1'],
				'doc2'=>$vec['documento2'],
				'doc3'=>$vec['documento3'],
				'doc4'=>$vec['documento4'],
				'doc5'=>$vec['documento5'],
				'doc6'=>$vec['documento6'],
				'doc7'=>$vec['documento7'],
				'doc8'=>$vec['documento8'],
				'corrugado'=>$vec['corrugado'],
				'piezascorrugado'=>$vec['piezasCorrugado'],
				'anchocorrugado'=>$vec['anchoCorrugado'],	
				'largocorrugado'=>$vec['largoCorrugado'],
				'altocorrugado'=>$vec['altoCorrugado'],
				'anchopallet'=>$vec['anchoPallet'],
				'largopallet'=>$vec['largoPallet'],
				'alturapallet'=>$vec['alturaPallet'],
				'corrugadosporcama'=>$vec['corrugadosPorCama'],	
				'corrugadosporpallet'=>$vec['corrugadosPorPallet'],
				'camasporpallet'=>$vec['camasPorPallet'],
				'piezasPorPallet'=>$vec['piezasPorPallet'],
				'pesopresentacion'=>$vec['pesoPresentacionVenta'],
				'pesoporcorrugado'=>$vec['pesoPorCorrugado'],
				'pesoporpallet'=>$vec['pesoPorPallet'],
				'anchoproducto'=>$vec['anchoProducto'],
				'largoproducto'=>$vec['largoProducto'],
				'altoproducto'=>$vec['altoProducto'],
				'pais'=>$vec['paisOrigen'],
				'fraccionarancelaria'=>$vec['fraccionArancelaria'],
				'exportacion'=>$vec['exportacion'],
				'notas'=>$vec['notasEspeciales'],
				'barniz'=>$vec['barniz'],
				'activo'=>$vec['activo']
				);
		
                }
			
	
	return $datos;
	}
public function getallproductsenable(){
		$datos=array();
		$result=$this->app->getrecord("productos","*"," where activo like '1' ORDER BY productos.nombreProducto ASC");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
		
			$datos[] = array(
				'id'=>$vec['id'],
				'nombre'=>$vec['nombreProducto'],
				'codigo'=>$vec['codigoProducto'],	
				'descripcion'=>$vec['descripcion'],
				'presentacion'=>$vec['presentacionVenta'],
				'marca'=>$vec['marcaProducto'],
				'codigobarrasproducto'=>$vec['codigoBarrasProducto'],
				'codigobarrasempaque'=>$vec['codigoBarrasEmpaque'],
				'foto'=>$vec['fotos'],
				'fotodetalle'=>$vec['fotosDetalle'],
				'doc1'=>$vec['documento1'],
				'doc2'=>$vec['documento2'],
				'doc3'=>$vec['documento3'],
				'doc4'=>$vec['documento4'],
				'doc5'=>$vec['documento5'],
				'doc6'=>$vec['documento6'],
				'doc7'=>$vec['documento7'],
				'doc8'=>$vec['documento8'],
				'corrugado'=>$vec['corrugado'],
				'piezascorrugado'=>$vec['piezasCorrugado'],
				'anchocorrugado'=>$vec['anchoCorrugado'],	
				'largocorrugado'=>$vec['largoCorrugado'],
				'altocorrugado'=>$vec['altoCorrugado'],
				'anchopallet'=>$vec['anchoPallet'],
				'largopallet'=>$vec['largoPallet'],
				'alturapallet'=>$vec['alturaPallet'],
				'corrugadosporcama'=>$vec['corrugadosPorCama'],	
				'corrugadosporpallet'=>$vec['corrugadosPorPallet'],
				'camasporpallet'=>$vec['camasPorPallet'],
				'piezasPorPallet'=>$vec['piezasPorPallet'],
				'pesopresentacion'=>$vec['pesoPresentacionVenta'],
				'pesoporcorrugado'=>$vec['pesoPorCorrugado'],
				'pesoporpallet'=>$vec['pesoPorPallet'],
				'anchoproducto'=>$vec['anchoProducto'],
				'largoproducto'=>$vec['largoProducto'],
				'altoproducto'=>$vec['altoProducto'],
				'pais'=>$vec['paisOrigen'],
				'fraccionarancelaria'=>$vec['fraccionArancelaria'],
				'exportacion'=>$vec['exportacion'],
				'notas'=>$vec['notasEspeciales'],
				'barniz'=>$vec['barniz'],
				'activo'=>$vec['activo']
				);
		
                }
			
	
	return $datos;
	}

	public function getspecificproduct($id){
		$datos=array();
		$result=$this->app->getrecord("productos","*"," where id=$id");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
		
			$datos[] = array(
				'id'=>$vec['id'],
				'nombre'=>$vec['nombreProducto'],
				'codigo'=>$vec['codigoProducto'],	
				'descripcion'=>$vec['descripcion'],
				'presentacion'=>$vec['presentacionVenta'],
				'marca'=>$vec['marcaProducto'],
				'codigobarrasproducto'=>$vec['codigoBarrasProducto'],
				'codigobarrasempaque'=>$vec['codigoBarrasEmpaque'],
				'foto'=>$vec['fotos'],
				'fotodetalle'=>$vec['fotosDetalle'],
				'doc1'=>$vec['documento1'],
				'doc2'=>$vec['documento2'],
				'doc3'=>$vec['documento3'],
				'doc4'=>$vec['documento4'],
				'doc5'=>$vec['documento5'],
				'doc6'=>$vec['documento6'],
				'doc7'=>$vec['documento7'],
				'doc8'=>$vec['documento8'],
				'corrugado'=>$vec['corrugado'],
				'piezascorrugado'=>$vec['piezasCorrugado'],
				'anchocorrugado'=>$vec['anchoCorrugado'],	
				'largocorrugado'=>$vec['largoCorrugado'],
				'altocorrugado'=>$vec['altoCorrugado'],
				'anchopallet'=>$vec['anchoPallet'],
				'largopallet'=>$vec['largoPallet'],
				'alturapallet'=>$vec['alturaPallet'],
				'corrugadosporcama'=>$vec['corrugadosPorCama'],	
				'corrugadosporpallet'=>$vec['corrugadosPorPallet'],
				'camasporpallet'=>$vec['camasPorPallet'],
				'piezasPorPallet'=>$vec['piezasPorPallet'],
				'pesopresentacion'=>$vec['pesoPresentacionVenta'],
				'pesoporcorrugado'=>$vec['pesoPorCorrugado'],
				'pesoporpallet'=>$vec['pesoPorPallet'],
				'anchoproducto'=>$vec['anchoProducto'],
				'largoproducto'=>$vec['largoProducto'],
				'altoproducto'=>$vec['altoProducto'],
				'pais'=>$vec['paisOrigen'],
				'fraccionarancelaria'=>$vec['fraccionArancelaria'],
				'exportacion'=>$vec['exportacion'],
				'notas'=>$vec['notasEspeciales'],
				'barniz'=>$vec['barniz'],
				'activo'=>$vec['activo']
				);
		
                }
			
	
	return $datos;
	}
	public function getspecificproductbysae($cad){
		$sae=explode("-",$cad);
		$datos=array();
		$result=$this->app->getrecord("productos","*"," where codigoProducto like '$sae[0]'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
		
			$datos[] = array(
				'id'=>$vec['id'],
				'nombre'=>$vec['nombreProducto'],
				'codigo'=>$vec['codigoProducto'],	
				'descripcion'=>$vec['descripcion'],
				'presentacion'=>$vec['presentacionVenta'],
				'marca'=>$vec['marcaProducto'],
				'codigobarrasproducto'=>$vec['codigoBarrasProducto'],
				'codigobarrasempaque'=>$vec['codigoBarrasEmpaque'],
				'foto'=>$vec['fotos'],
				'fotodetalle'=>$vec['fotosDetalle'],
				'doc1'=>$vec['documento1'],
				'doc2'=>$vec['documento2'],
				'doc3'=>$vec['documento3'],
				'doc4'=>$vec['documento4'],
				'doc5'=>$vec['documento5'],
				'doc6'=>$vec['documento6'],
				'doc7'=>$vec['documento7'],
				'doc8'=>$vec['documento8'],
				'corrugado'=>$vec['corrugado'],
				'piezascorrugado'=>$vec['piezasCorrugado'],
				'anchocorrugado'=>$vec['anchoCorrugado'],	
				'largocorrugado'=>$vec['largoCorrugado'],
				'altocorrugado'=>$vec['altoCorrugado'],
				'anchopallet'=>$vec['anchoPallet'],
				'largopallet'=>$vec['largoPallet'],
				'alturapallet'=>$vec['alturaPallet'],
				'corrugadosporcama'=>$vec['corrugadosPorCama'],	
				'corrugadosporpallet'=>$vec['corrugadosPorPallet'],
				'camasporpallet'=>$vec['camasPorPallet'],
				'piezasPorPallet'=>$vec['piezasPorPallet'],
				'pesopresentacion'=>$vec['pesoPresentacionVenta'],
				'pesoporcorrugado'=>$vec['pesoPorCorrugado'],
				'pesoporpallet'=>$vec['pesoPorPallet'],
				'anchoproducto'=>$vec['anchoProducto'],
				'largoproducto'=>$vec['largoProducto'],
				'altoproducto'=>$vec['altoProducto'],
				'pais'=>$vec['paisOrigen'],
				'fraccionarancelaria'=>$vec['fraccionArancelaria'],
				'exportacion'=>$vec['exportacion'],
				'notas'=>$vec['notasEspeciales'],
				'barniz'=>$vec['barniz'],
				'activo'=>$vec['activo'],
				'procesos'=>$this->getprocesosproducto($vec['id'])
				);
		
                }
			
	
	return $datos;
	}
	public function updateproducto($codigo,$descripcion,$nombre,$presentacion,$marca,$codigobarras,$codigobarrasempaque,$pais,$fraccionarancelaria,$exportacion,$barniz,$foto,$fotodetalle,$notasespeciales,$corrugado,$corrugadospallet,$piezascorrugado,$camaspallete,$anchocorrugado,$piezaspallet,$largocorrugado,$pesopresentacion,$altrocorrugado,$pesocorrugado,$anchopallet,$pesopallete,$largopallet,$anchoproducto,$alturapallet,$largoproducto,$corrugadosporcama,$altoproducto){
	//trigger_error($vendedor);
		$datos=array();
		$result=$this->app->getrecord("productos","*"," where codigoProducto like '$codigo'");
		
		if (sizeof($result) > 0) {
		
			if($this->app->updaterecord("productos","nombreProducto='$nombre',descripcion='$descripcion',presentacionVenta='$presentacion',marcaProducto='$marca',codigoBarrasProducto='$codigobarras',codigoBarrasEmpaque='$codigobarrasempaque',fotos='$foto',fotosDetalle='$fotodetalle',corrugado='$corrugado',piezasCorrugado=$piezascorrugado,anchoCorrugado=$anchocorrugado,largoCorrugado=$largocorrugado,altoCorrugado=$altrocorrugado,anchoPallet=$anchopallet,largoPallet=$largopallet,alturaPallet=$alturapallet,corrugadosPorCama=$corrugadosporcama,corrugadosPorPallet=$corrugadospallet,camasPorPallet=$camaspallete,piezasPorPallet=$piezaspallet,pesoPresentacionVenta=$pesopresentacion,pesoPorCorrugado=$pesocorrugado,pesoPorPallet=$pesopallete,anchoProducto=$anchoproducto,largoProducto=$largoproducto,altoProducto=$altoproducto,paisOrigen='$pais',fraccionArancelaria='$fraccionarancelaria',exportacion='$exportacion',notasEspeciales='$notasespeciales',barniz='$barniz'"," where codigoProducto like '$codigo'")){
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);
							
				$result=$this->app->getrecord("paths","*"," where codigoProducto like '$codigo' and tipo like 'U'");
				if(sizeof($result)>0){
					for($a=0;$a<sizeof($result);$a++){
						$vec=$result[$a];
						
						switch($vec['archivo']){
							case "doc1";
								if($this->app->updaterecord("productos","documento1='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern = $this->path ."doc1-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc1-".$codigo."U.".$ext[1],$this->path."doc1-".$codigo.".".$ext[1]);
								}
								
							break;							
							case "doc2";
								if($this->app->updaterecord("productos","documento2='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern = $this->path ."doc2-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc2-".$codigo."U.".$ext[1],$this->path."doc2-".$codigo.".".$ext[1]);
								}							break;							
							case "doc3";
								if($this->app->updaterecord("productos","documento3='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern =$this->path ."doc3-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc3-".$codigo."U.".$ext[1], $this->path."doc3-".$codigo.".".$ext[1]);
								}
							break;							
							case "doc4";
								if($this->app->updaterecord("productos","documento4='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern = $this->path ."doc4-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc4-".$codigo."U.".$ext[1], $this->path."doc4-".$codigo.".".$ext[1]);
								}
							break;							
							case "doc5";
								if($this->app->updaterecord("productos","documento5='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern = $this->path ."doc5-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc5-".$codigo."U.".$ext[1], $this->path."doc5-".$codigo.".".$ext[1]);
								}
							break;							
							case "doc6";
								if($this->app->updaterecord("productos","documento6='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern = $this->path ."doc6-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc6-".$codigo."U.".$ext[1], $this->path."doc6-".$codigo.".".$ext[1]);
								}
							break;							
							case "doc7";
								if($this->app->updaterecord("productos","documento7='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern =$this->path ."doc7-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc7-".$codigo."U.".$ext[1], $this->path."doc7-".$codigo.".".$ext[1]);
								}
							break;							
							case "doc8";
								if($this->app->updaterecord("productos","documento8='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern = $this->path ."doc8-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc8-".$codigo."U.".$ext[1], $this->path."doc8-".$codigo.".".$ext[1]);
								}
							break;							


						}
						
					}
				}
							
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}

	$this->app->deleterecord('paths',"where codigoProducto like '$codigo'");
	return $datos;	

	}


	public function updateproductoenable($codigo,$descripcion,$nombre,$presentacion,$marca,$codigobarras,$codigobarrasempaque,$pais,$fraccionarancelaria,$exportacion,$barniz,$foto,$fotodetalle,$notasespeciales,$corrugado,$corrugadospallet,$piezascorrugado,$camaspallete,$anchocorrugado,$piezaspallet,$largocorrugado,$pesopresentacion,$altrocorrugado,$pesocorrugado,$anchopallet,$pesopallete,$largopallet,$anchoproducto,$alturapallet,$largoproducto,$corrugadosporcama,$altoproducto,$activo){
	//trigger_error($codigo);
		$datos=array();
		$result=$this->app->getrecord("productos","*"," where codigoProducto like '$codigo'");
		
		if (sizeof($result) > 0) {
		
			if($this->app->updaterecord("productos","nombreProducto='$nombre',descripcion='$descripcion',presentacionVenta='$presentacion',marcaProducto='$marca',codigoBarrasProducto='$codigobarras',codigoBarrasEmpaque='$codigobarrasempaque',fotos='$foto',fotosDetalle='$fotodetalle',corrugado='$corrugado',piezasCorrugado=$piezascorrugado,anchoCorrugado=$anchocorrugado,largoCorrugado=$largocorrugado,altoCorrugado=$altrocorrugado,anchoPallet=$anchopallet,largoPallet=$largopallet,alturaPallet=$alturapallet,corrugadosPorCama=$corrugadosporcama,corrugadosPorPallet=$corrugadospallet,camasPorPallet=$camaspallete,piezasPorPallet=$piezaspallet,pesoPresentacionVenta=$pesopresentacion,pesoPorCorrugado=$pesocorrugado,pesoPorPallet=$pesopallete,anchoProducto=$anchoproducto,largoProducto=$largoproducto,altoProducto=$altoproducto,paisOrigen='$pais',fraccionArancelaria='$fraccionarancelaria',exportacion='$exportacion',notasEspeciales='$notasespeciales',barniz='$barniz',activo='$activo'"," where codigoProducto like '$codigo'")){
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);
							
				$result=$this->app->getrecord("paths","*"," where codigoProducto like '$codigo' and tipo like 'U'");
				if(sizeof($result)>0){
					for($a=0;$a<sizeof($result);$a++){
						$vec=$result[$a];
						
						switch($vec['archivo']){
							case "doc1";
								if($this->app->updaterecord("productos","documento1='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern = $this->path ."doc1-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc1-".$codigo."U.".$ext[1], $this->path."doc1-".$codigo.".".$ext[1]);
								}
								
							break;							
							case "doc2";
								if($this->app->updaterecord("productos","documento2='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern =$this->path ."doc2-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc2-".$codigo."U.".$ext[1], $this->path."doc2-".$codigo.".".$ext[1]);
								}							
							break;							
							case "doc3";
								if($this->app->updaterecord("productos","documento3='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern = $this->path."doc3-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc3-".$codigo."U.".$ext[1], $this->path."doc3-".$codigo.".".$ext[1]);
								}
							break;							
							case "doc4";
								if($this->app->updaterecord("productos","documento4='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern =$this->path ."doc4-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc4-".$codigo."U.".$ext[1], $this->path."doc4-".$codigo.".".$ext[1]);
								}
							break;							
							case "doc5";
								if($this->app->updaterecord("productos","documento5='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern = $this->path ."doc5-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc5-".$codigo."U.".$ext[1], $this->path."doc5-".$codigo.".".$ext[1]);
								}
							break;							
							case "doc6";
								if($this->app->updaterecord("productos","documento6='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern = $this->path ."doc6-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc6-".$codigo."U.".$ext[1], $this->path."doc6-".$codigo.".".$ext[1]);
								}
							break;							
							case "doc7";
								if($this->app->updaterecord("productos","documento7='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern = $this->path ."doc7-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc7-".$codigo."U.".$ext[1], $this->path."doc7-".$codigo.".".$ext[1]);
								}
							break;							
							case "doc8";
								if($this->app->updaterecord("productos","documento8='$vec[ruta]'"," where codigoProducto like '$codigo'")){
									$ext=explode(".",$vec['ruta']);
									$file_pattern =$this->path ."doc8-".$codigo.".*";
									array_map( "unlink", glob( $file_pattern ) );
									rename ($this->path."doc8-".$codigo."U.".$ext[1], $this->path."doc8-".$codigo.".".$ext[1]);
								}
							break;							


						}
						
					}
				}
				
							
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}

	$this->app->deleterecord('paths',"where codigoProducto like '$codigo'");
	return $datos;	
	}

	public function deleteproducto($id){
		$datos=array();
		$result=$this->app->getrecord("productos","*"," where id=$id");
		
		if (sizeof($result) > 0) 
		{		
			if($this->app->updaterecord("productos","activo='0'"," where id=$id")){
					$datos[]=array(
						'exito'=>'si'
					);					
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin'

				);
		}


	return $datos;	
	}
	public function duplicateproducto ($idold,$idnew,$nombrenew){
		$datos=array();
		$result=$this->app->getrecord("productos","*","where codigoProducto like '$idnew'");
				
		if (sizeof($result) > 0) {
			$datos[]=array(
				'exito'=>'ya',
				'folio'=>0
			);
		} 
		else 
		{
			$result=$this->app->getrecord("productos","*"," where codigoProducto like '$idold'");
			for($a=0;$a<sizeof($result);$a++)
    			{
				$vec=$result[$a];
		
				
				$aux=explode("-",$vec['documento1']);
				$aux2=explode(".",$aux[1]);
				$doc1=$aux[0]."-".$idnew.".".$aux2[1];
				$file = $this->path2.$vec['documento1'];
				$newfile =$this->path2.$doc1;
				copy($file, $newfile);
 				

				$aux=explode("-",$vec['documento2']);
				$aux2=explode(".",$aux[1]);
				$doc2=$aux[0]."-".$idnew.".".$aux2[1];
				$file = $this->path2.$vec['documento2'];
				$newfile =$this->path2.$doc2;
				copy($file, $newfile);
 				
				$aux=explode("-",$vec['documento3']);
				$aux2=explode(".",$aux[1]);
				$doc3=$aux[0]."-".$idnew.".".$aux2[1];
				$file = $this->path2.$vec['documento3'];
				$newfile =$this->path2.$doc3;
				copy($file, $newfile);
 				
				$aux=explode("-",$vec['documento4']);
				$aux2=explode(".",$aux[1]);
				$doc4=$aux[0]."-".$idnew.".".$aux2[1];
				$file = $this->path2.$vec['documento4'];
				$newfile =$this->path2.$doc4;
				copy($file, $newfile);
 				
				$aux=explode("-",$vec['documento5']);
				$aux2=explode(".",$aux[1]);
				$doc5=$aux[0]."-".$idnew.".".$aux2[1];
				$file = $this->path2.$vec['documento5'];
				$newfile =$this->path2.$doc5;
				copy($file, $newfile);
 				
				$aux=explode("-",$vec['documento6']);
				$aux2=explode(".",$aux[1]);
				$doc6=$aux[0]."-".$idnew.".".$aux2[1];
				$file = $this->path2.$vec['documento6'];
				$newfile =$this->path2.$doc6;
				copy($file, $newfile);
 				
				$aux=explode("-",$vec['documento7']);
				$aux2=explode(".",$aux[1]);
				$doc7=$aux[0]."-".$idnew.".".$aux2[1];
				$file =$this->path2.$vec['documento7'];
				$newfile =$this->path2.$doc7;
				copy($file, $newfile);
 				
				$aux=explode("-",$vec['documento8']);
				$aux2=explode(".",$aux[1]);
				$doc8=$aux[0]."-".$idnew.".".$aux2[1];
				$file =$this->path2.$vec['documento8'];
				$newfile =$this->path2.$doc8;
				copy($file, $newfile);
 				
				
					

				
				$id=$this->app->saverecord("productos","null,'$nombrenew','$idnew','$vec[descripcion]','$vec[presentacionVenta]','$vec[marcaProducto]','$vec[codigoBarrasProducto]','$vec[codigoBarrasEmpaque]','$vec[fotos]','$vec[fotosDetalle]','$doc1','$doc2','$doc3','$doc4','$doc5','$doc6','$doc7','$doc8','$vec[corrugado]',$vec[piezasCorrugado],$vec[anchoCorrugado],$vec[largoCorrugado],$vec[altoCorrugado],$vec[anchoPallet],$vec[largoPallet],$vec[alturaPallet],$vec[corrugadosPorCama],$vec[corrugadosPorPallet],$vec[camasPorPallet],$vec[piezasPorPallet],$vec[pesoPresentacionVenta],$vec[pesoPorCorrugado],$vec[pesoPorPallet],$vec[anchoProducto],$vec[largoProducto],$vec[altoProducto],'$vec[paisOrigen]','$vec[fraccionArancelaria]','$vec[exportacion]','$vec[notasEspeciales]','$vec[barniz]','1'");			
				if($id>0 ){
					$result2=$this->app->getrecord("procesos","*"," where idProducto= $vec[id]");
					for($aa=0;$aa<sizeof($result2);$aa++)
    					{
						$vec2=$result2[$aa];
						$id2=$this->app->saverecord("procesos","null,$id,'$vec2[categoria]','$vec2[descripcionProceso]','$vec2[subensamble]','$vec2[proceso]',$vec2[maquina],'$vec2[medidaEntrada]','$vec2[piezasFormato]','$vec2[mermaAjuste]','$vec2[estandarProcesoHora]','$vec2[estandarAjuste]','$vec2[estandarCalculado]','$vec2[especificacion1]','$vec2[especificacion2]','$vec2[especificacion3]','$vec2[especificacion4]','$vec2[especificacion5]','$vec2[multiploProceso]','','','',$vec2[horasTurno],'$vec2[estandarTurno]','','','','1'");			
		
					}			
					$result2=$this->app->getrecord("relproducto_componente","*"," where idProducto= $vec[id]");
					for($aa=0;$aa<sizeof($result2);$aa++)
    					{
						$vec2=$result2[$aa];
						$id3=$this->app->saverecord("relproducto_componente","null,$id,$vec2[idComponente],$vec2[cantidad],'$vec2[insumo]','1'");			
		
					}



					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);	
				}else{
					$datos[]=array(
						'exito'=>'no',
						'folio'=>0
					);	
				}	
		
                	}
		}
			
	

	return $datos;	
	}
	public function getallmaquinas(){
		$datos=array();
		$result=$this->app->getrecord("maquinas","*","where activo like '1'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];	

			$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre']
					);			
                }
		
		
	return $datos; 
	}
	public function getmaquinadetail ($id){
		$datos=array();
		$result=$this->app->getrecord("maquinas","*","where id=$id and activo like '1'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];	

			$datos[] = array(
					'id'=>$vec['id'],
					'nombre'=>$vec['nombre'],
					'proceso'=>$vec['proceso'],
					'estandar'=>$vec['estandar'],
					'medida'=>$vec['unidad_medida']
					);			
                }
		
		
	return $datos; 
	} 
	public function saveproceso ($idproducto,$maquina,$proceso,$categoria,$subensamble,$descripcion,$medidaentrada,$piezasforma,$mermaajuste,$estandarproceso,$estandarajuste,$estandarcalculado,$espec1,$espec2,$espec3,$espec4,$espec5,$multiploproceso,$horas,$estandarturno){
	$datos=array();	
			$categoria=ucfirst ($categoria);
		$result2=$this->app->getrecord("categorias","*","where nombre like '$categoria'");
			//trigger_error(sizeof($result2));
			if(sizeof($result2)<=0){
				
				$this->app->saverecord("categorias","null,'$categoria','','1'");
			}
		$subensamble=ucfirst ($subensamble);
		$result2=$this->app->getrecord("subensambles","*","where nombre like '$subensamble'");
			//trigger_error(sizeof($result2));
			if(sizeof($result2)<=0){
				
				$this->app->saverecord("subensambles","null,'$subensamble','','1'");
			}

		$id=$this->app->saverecord("procesos","null,$idproducto,'$categoria','$descripcion','$subensamble','$proceso',$maquina,'$medidaentrada','$piezasforma','$mermaajuste','$estandarproceso','$estandarajuste','$estandarcalculado','$espec1','$espec2','$espec3','$espec4','$espec5','$multiploproceso','','','',$horas,'$estandarturno','','','','1'");			
				if($id>0 ){
			
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);	
				}else{
					$datos[]=array(
						'exito'=>'no',
						'folio'=>0
					);	
				}	
	return $datos;
	}
	
	public function getprocesosproducto($idproducto){
	$datos=array();
		$result=$this->app->getrecord("procesos","procesos.id,procesos.idProducto,procesos.categoria,procesos.descripcionProceso,procesos.subensamble,procesos.proceso,procesos.maquina,procesos.medidaEntrada,procesos.piezasFormato,procesos.mermaAjuste,procesos.estandarProcesoHora,procesos.estandarAjuste,procesos.estandarCalculado,procesos.especificacion1,procesos.especificacion2,procesos.especificacion3,procesos.especificacion4,procesos.especificacion5,procesos.multiploProceso,procesos.cantidadOrden,procesos.cantidadProducto,procesos.cantidadProceso,procesos.horasTurno,procesos.estandarTurno,procesos.cantidadTurno,procesos.cantidadTickets,procesos.horasRequeridas,procesos.activo,maquinas.nombre","inner join maquinas on maquinas.id=procesos.maquina where procesos.idProducto=$idproducto");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
		
			$datos[] = array(
				'id'=>$vec['id'],
				'idproducto'=>$vec['idProducto'],
				'categoria'=>$vec['categoria'],	
				'maquina'=>$vec['nombre'],
				'descripcion'=>$vec['descripcionProceso'],
				'subensamble'=>$vec['subensamble'],
				'proceso'=>$vec['proceso'],
				'maquinaid'=>$vec['maquina'],
				'medidaentrada'=>$vec['medidaEntrada'],
				'piezasformato'=>$vec['piezasFormato'],
				'mermaajuste'=>$vec['mermaAjuste'],
				'estandarhora'=>$vec['estandarProcesoHora'],
				'estandarajuste'=>$vec['estandarAjuste'],
				'estandarcalculado'=>$vec['estandarCalculado'],
				'esp1'=>$vec['especificacion1'],
				'esp2'=>$vec['especificacion2'],
				'esp3'=>$vec['especificacion3'],
				'esp4'=>$vec['especificacion4'],
				'esp5'=>$vec['especificacion5'],
				'multiploproceso'=>$vec['multiploProceso'],
				'cantidadorden'=>$vec['cantidadOrden'],
				'cantidadproducto'=>$vec['cantidadProducto'],	
				'cantidadproceso'=>$vec['cantidadProceso'],
				'horas'=>$vec['horasTurno'],
				'estandarturno'=>$vec['estandarTurno'],
				'cantidadturno'=>$vec['cantidadTurno'],
				'cantidadtickets'=>$vec['cantidadTickets'],
				'horasrequeridas'=>$vec['horasRequeridas'],	
				'activo'=>$vec['activo']
				);
		
                }
			
	
	return $datos;
	}

	public function deleterecordproceso ($id){
		$datos=array();
		$result=$this->app->getrecord("procesos","*"," where id=$id");
		
		if (sizeof($result) > 0) 
		{		
			if($this->app->deleterecord("procesos"," where id=$id")>0){
					$datos[]=array(
						'exito'=>'si'
					);					
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin'

				);
		}


	return $datos;

	}
	public function saveprocesoedita ($id,$maquina,$proceso,$categoria,$subensamble,$descripcion,$medidaentrada,$piezasforma,$mermaajuste,$estandarproceso,$estandarajuste,$estandarcalculado,$espec1,$espec2,$espec3,$espec4,$espec5,$multiploproceso,$horas,$estandarturno){
		$datos=array();
		$result=$this->app->getrecord("procesos","*"," where id=$id");
		
		if (sizeof($result) > 0) {
		
			if($this->app->updaterecord("procesos","maquina=$maquina,proceso='$proceso',categoria='$categoria',subensamble='$subensamble',descripcionProceso='$descripcion',medidaEntrada='$medidaentrada',piezasFormato='$piezasforma',mermaAjuste='$mermaajuste',estandarProcesoHora='$estandarproceso',estandarAjuste='$estandarajuste',estandarCalculado='$estandarcalculado',especificacion1='$espec1',especificacion2='$espec2',especificacion3='$espec3',especificacion4='$espec4',especificacion5='$espec5',multiploProceso='$multiploproceso',horasTurno=$horas,estandarTurno='$estandarturno'"," where id=$id")){
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);			
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}
	return $datos;	
	}
	public function getcomponentes(){
	$datos=array();
		$result=$this->app->getrecord("componentessae","*","");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
		
			$datos[] = array(
				'id'=>$vec['id'],
				'clave'=>$vec['clave'],
				'componente'=>$vec['descripcion'],	
				'linea'=>$vec['linea'],
				'unidadEntrada'=>$vec['unidadEntrada'],
				'ultimoCosto'=>$vec['ultimoCosto'],
				'cantidad'=>$vec['cantidad'],
				'paisOrigen'=>$vec['paisOrigen'],
				'merma'=>$vec['merma'],
				'fraccionArancelaria'=>$vec['fraccionArancelaria']
				);
		
                }
			
	
	return $datos;
	}

	public function getspecificcomponente($cad){
	$datos=array();
		$sae=explode("-",$cad);
		//trigger_error($sae[0]);
		$result=$this->app->getrecord("componentessae","*","where clave like '$sae[0]'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos[] = array(
				'id'=>$vec['id'],
				'clave'=>$vec['clave'],
				'componente'=>$vec['descripcion'],	
				'linea'=>$vec['linea'],
				'unidadEntrada'=>$vec['unidadEntrada'],
				'ultimoCosto'=>$vec['ultimoCosto'],
				'cantidad'=>$vec['cantidad'],
				'paisOrigen'=>$vec['paisOrigen'],
				'merma'=>$vec['merma'],
				'fraccionArancelaria'=>$vec['fraccionArancelaria']
				);
                }
	return $datos;
	}
	public function savecomponente($idproducto,$cantidad,$componente){
		$datos=array();	

		

		$id=$this->app->saverecord("relproducto_componente","null,$idproducto,$componente,$cantidad,'','1'");			
				if($id>0 ){
			
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);	
				}else{
					$datos[]=array(
						'exito'=>'no',
						'folio'=>0
					);	
				}	
	return $datos;
	
	}
	public function getcomponentesproducto($idproducto){
	$datos=array();
		
		$result=$this->app->getrecord("relproducto_componente","relproducto_componente.id,relproducto_componente.idComponente,componentessae.clave,componentessae.descripcion,componentessae.linea,componentessae.unidadEntrada,componentessae.ultimoCosto,relproducto_componente.cantidad","INNER JOIN componentessae on relproducto_componente.idComponente=componentessae.id WHERE relproducto_componente.idProducto=$idproducto");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos[] = array(
				'id'=>$vec['id'],
				'idcomponente'=>$vec['idComponente'],
				'clave'=>$vec['clave'],
				'componente'=>$vec['descripcion'],	
				'linea'=>$vec['linea'],
				'unidadEntrada'=>$vec['unidadEntrada'],
				'ultimoCosto'=>$vec['ultimoCosto'],
				'cantidad'=>$vec['cantidad']
				);
                }
	return $datos;
	}

	public function deleterecordcomponente ($id){
		$datos=array();
		$result=$this->app->getrecord("relproducto_componente","*"," where id=$id");
		
		if (sizeof($result) > 0) 
		{		
			if($this->app->deleterecord("relproducto_componente"," where id=$id")>0){
					$datos[]=array(
						'exito'=>'si'
					);					
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin'

				);
		}


	return $datos;

	}

	public function savecomponenteedita($id,$cantidad,$idcomponente){
		$datos=array();
		$result=$this->app->getrecord("relproducto_componente","*"," where id=$id");
		
		if (sizeof($result) > 0) {
		
			if($this->app->updaterecord("relproducto_componente","cantidad=$cantidad,idComponente=$idcomponente"," where id=$id")){
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);			
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}
	return $datos;	


	} 
	public function saveorden($pedidosae,$fecharequerida,$producto,$cantidad){
	$datos=array();			
		$vec=$this->getspecificproductbysae($producto);
		$idd=$vec[0]['id'];
		//trigger_error(print_r($vec,true));
		$id=$this->app->saverecord("ordenmaestra","null,$pedidosae,$idd,Now(),'$fecharequerida',$cantidad,$cantidad,0,5,'1'");			
				if($id>0 ){
			
					$vec2=$this->getprocesosproducto($idd);
					//trigger_error(print_r($vec2,true));
					//trigger_error(sizeof($vec2)."--".$vec2[0]['id']);
					
					for($a=0;$a<sizeof($vec2);$a++){
						$idproceso=$vec2[$a]['id'];
						$id12=$this->app->saverecord("rel_procesosorden","null,$id,$idproceso,2,0,'','','','',0,0,'','','','','1'");	
					}
					$vec2=$this->getcomponentesproducto($idd);
		
					for($a=0;$a<sizeof($vec2);$a++){
						$idcomponente=$vec2[$a]['id'];
						$cantidadorden=$cantidad*$vec2[$a]['cantidad'];
						$total=$cantidadorden*$vec2[$a]['ultimoCosto'];
						$id12=$this->app->saverecord("rel_componentesorden","null,$id,$idcomponente,$cantidadorden,$total,'1'");	
					}

					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);	
				}else{
					$datos[]=array(
						'exito'=>'no',
						'folio'=>0
					);	
				}	
	return $datos;

	}
	public function getallorders(){
	$datos=array();

	$result=$this->app->getrecord("ordenmaestra","ordenmaestra.id,ordenmaestra.pedidoSAE,productos.nombreProducto,ordenmaestra.fechaEmision,ordenmaestra.fechaRequerida,ordenmaestra.cantidad,ordenmaestra.faltante,ordenmaestra.producido,status_orden.status","INNER JOIN productos on productos.id=ordenmaestra.idProducto INNER JOIN status_orden on status_orden.id=ordenmaestra.idStatus where ordenmaestra.activo like '1' order by ordenmaestra.fechaEmision desc");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos[] = array(
				'id'=>$vec['id'],
				'producto'=>$vec['nombreProducto'],
				'fechaemision'=>$vec['fechaEmision'],	
				'fecharequerida'=>$vec['fechaRequerida'],
				'cantidad'=>$vec['cantidad'],
				'faltante'=>$vec['faltante'],
				'producido'=>$vec['producido'],
				'status'=>$vec['status'],
				'pedidosae'=>$vec['pedidoSAE'],
				'activo'=>$vec['activo']
				);
                }
	
	return $datos;
	}
	public function getspecificorder($id){
	$datos=array();

	$result=$this->app->getrecord("ordenmaestra","ordenmaestra.id,ordenmaestra.pedidoSAE,ordenmaestra.idProducto,productos.nombreProducto,productos.descripcion,ordenmaestra.fechaEmision,ordenmaestra.fechaRequerida,ordenmaestra.cantidad,ordenmaestra.faltante,ordenmaestra.producido,status_orden.status","INNER JOIN productos on productos.id=ordenmaestra.idProducto INNER JOIN status_orden on status_orden.id=ordenmaestra.idStatus where ordenmaestra.activo like '1' and ordenmaestra.id=$id");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos[] = array(
				'id'=>$vec['id'],
				'idproducto'=>$vec['idProducto'],
				'producto'=>$vec['nombreProducto'],
				'fechaemision'=>$vec['fechaEmision'],	
				'fecharequerida'=>$vec['fechaRequerida'],
				'descripcion'=>$vec['descripcion'],
				'cantidad'=>$vec['cantidad'],
				'faltante'=>$vec['faltante'],
				'producido'=>$vec['producido'],
				'status'=>$vec['status'],
				'pedidosae'=>$vec['pedidoSAE'],
				'activo'=>$vec['activo']
				);
                }
	
	return $datos;
	}

	public function getordersbyoperator($idoperator){
	
	$datos=array();
	//trigger_error($idoperator);
	$result=$this->app->getrecord("rel_procesosorden","DISTINCT idOrden"," where idOperador=$idoperator");
	 //trigger_error(print_r($result,true));
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$idorden=$vec['idOrden'];
			
			$datos[] = array(
				$this->getspecificorder($idorden)
				);
                }
	
	return $datos;
	}	

	public function getstatus(){
	$datos=array();

	$result=$this->app->getrecord("status_orden","*","where activo like '1'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos[] = array(
				'id'=>$vec['id'],
				'status'=>$vec['status'],
				'extra'=>$vec['extra'],	
				'activo'=>$vec['activo']
				);
                }
	
	return $datos;	
	}
	public function getstatusbyid($id){
	$datos=array();

	$result=$this->app->getrecord("status_orden","*","where activo like '1' and id=$id");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos[] = array(
				'id'=>$vec['id'],
				'status'=>$vec['status'],
				'extra'=>$vec['extra'],	
				'activo'=>$vec['activo']
				);
                }
	
	return $datos;	
	}
	public function updateorder ($id,$fecharequerida,$cantidad,$status){
	$datos=array();
		$result=$this->app->getrecord("ordenmaestra","*"," where id=$id");
		
		if (sizeof($result) > 0) {
			$vec=$result[0];
			$producido=$vec['producido'];

			

			if($this->app->updaterecord("ordenmaestra"," fechaRequerida='$fecharequerida',cantidad=$cantidad,faltante=$cantidad-$producido,idStatus=$status","where id=$id")){

				$result3=$this->app->getrecord("rel_componentesorden","rel_componentesorden.id,relproducto_componente.cantidad,componentessae.ultimoCosto","  inner join relproducto_componente on relproducto_componente.id=rel_componentesorden.idrelacionComponente INNER join componentessae on componentessae.id=relproducto_componente.idComponente where rel_componentesorden.idOrden=$id");
				//trigger_error(print_r($result3,true));
				for($r=0;$r<sizeof($result3);$r++){
					$vec4=$result3[$r];
					$idmodificar=$vec4['id'];

					$cantidadunitaria=$vec4['cantidad'];
					$cantidadtotal=$cantidadunitaria*$cantidad;
					$costo=$cantidadtotal*$vec4['ultimoCosto'];
					//trigger_error('unitario->'.$cantidadunitaria.'total->'.$cantidadtotal.'costo total->'.$costo);
					$this->app->updaterecord("rel_componentesorden"," cantidadOrden=$cantidadtotal,costoTotal=$costo","where id=$idmodificar");			

					
				}

					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);		
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}

	return $datos;	
	}
	public function deleterecordorder($id){
	$datos=array();
		$result=$this->app->getrecord("ordenmaestra","*"," where id=$id");
		
		if (sizeof($result) > 0) 
		{		
			if($this->app->updaterecord("ordenmaestra","activo='0'"," where id=$id")){
				$datos[]=array(
					'exito'=>'si'
				);					
			}			
		} 
		else{
			$datos[]=array(
				'exito'=>'sin'
				);
		}
	return $datos;	
	}
	public function detailcomponentsbyorder($id){
	$datos=array();
		$result=$this->app->getrecord("rel_componentesorden","rel_componentesorden.id,rel_componentesorden.activo,rel_componentesorden.cantidadOrden,rel_componentesorden.costoTotal,componentessae.clave,componentessae.linea,componentessae.descripcion,componentessae.unidadEntrada,componentessae.ultimoCosto,relproducto_componente.cantidad","inner JOIN relproducto_componente on relproducto_componente.id=rel_componentesorden.idrelacionComponente inner JOIN componentessae on componentessae.id=relproducto_componente.idComponente where rel_componentesorden.idOrden=$id");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos[] = array(
				'id'=>$vec['id'],
				'cantidadorden'=>$vec['cantidadOrden'],
				'costototal'=>$vec['costoTotal'],	
				'clave'=>$vec['clave'],
				'descripcion'=>$vec['descripcion'],	
				'um'=>$vec['unidadEntrada'],
				'ultimocosto'=>$vec['ultimoCosto'],
				'cantidadunitaria'=>$vec['cantidad'],
				'linea'=>$vec['linea'],
				'activo'=>$vec['activo']
				);
                }


	return $datos;				
	}
public function detailprocessbyorder($id){
	$datos=array();
	$operador;
		$result=$this->app->getrecord("rel_procesosorden","procesos.estandarTurno,rel_procesosorden.id,rel_procesosorden.idOperador,rel_procesosorden.tiempoAjuste,rel_procesosorden.fechafinEstimada,rel_procesosorden.hrfinEstimada,maquinas.nombre as 'maquina',maquinas.proceso,maquinas.unidad_medida,rel_procesosorden.horaRequerida,rel_procesosorden.fechaRequerida,rel_procesosorden.cantidadUtil,rel_procesosorden.merma,rel_procesosorden.fechaInicio,rel_procesosorden.horaInicio,rel_procesosorden.fechaTermino,rel_procesosorden.horaTermino ","inner JOIN procesos on procesos.id=rel_procesosorden.idProceso INNER JOIN maquinas on maquinas.id=procesos.maquina WHERE rel_procesosorden.idOrden=$id");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$order=$vec['id'];
			$result2=$this->app->getrecord("rel_procesosorden","concat(persona.nombre,' ',persona.primerApellido,' ',persona.segundoApellido) as 'operador'"," INNER JOIN usuarios ON usuarios.id=rel_procesosorden.idOperador INNER JOIN persona on persona.id=usuarios.idPersona where rel_procesosorden.id=$order ");
			$vec2=$result2[0];
			$operador=$vec2['operador'];
			

			$datos[] = array(
				'id'=>$vec['id'],
				'maquina'=>$vec['maquina'],
				'tiempoajuste'=>$vec['tiempoAjuste'],
				'fechafinestimada'=>$vec['fechafinEstimada'],
				'hrfinestimada'=>$vec['hrfinEstimada'],
				'proceso'=>$vec['proceso'],
				'estandarturno'=>$vec['estandarTurno'],	
				'um'=>$vec['unidad_medida'],
				'horarequerida'=>$vec['horaRequerida'],	
				'fecharequerida'=>$vec['fechaRequerida'],
				'cantidadutil'=>$vec['cantidadUtil'],
				'merma'=>$vec['merma'],
				'fechainicio'=>$vec['fechaInicio'],
				'horainicio'=>$vec['horaInicio'],
				'fechafin'=>$vec['fechaTermino'],
				'horafin'=>$vec['horaTermino'],
				'idoperador'=>$vec['idOperador'],
				'operador'=>$operador,
				'activo'=>$vec['activo']
				);
                }


	return $datos;				
	}
	public function detailorder($id){
	$datos=array();
		$result=$this->app->getrecord("ordenmaestra","*"," where id=$id");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos[] = array(
				'id'=>$vec['id'],
				'procesos'=>$this->detailprocessbyorder($id),
				'componentes'=>$this->detailcomponentsbyorder($id),	
				'detalleproducto'=>$this->getspecificproduct($vec['idProducto']),
				'fechaemision'=>$vec['fechaEmision'],	
				'pedidosae'=>$vec['pedidoSAE'],
				'fecharequerida'=>$vec['fechaRequerida'],
				'cantidad'=>$vec['cantidad'],
				'status'=>$this->getstatusbyid($vec['idStatus']),
				'activo'=>$vec['activo']
				);
                }


	return $datos;
	}

	public function getoperators(){
		$datos=array();
		$result=$this->app->getrecord("usuarios","usuarios.id,concat(persona.nombre,' ',persona.primerApellido,' ',persona.segundoApellido) as 'operador'"," inner join persona on persona.id=usuarios.idPersona where usuarios.operador like '1' and usuarios.activo like'1'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos[] = array(
				'id'=>$vec['id'],
				'operador'=>$vec['operador']
				);
                }


	return $datos;
	}
	public function updateprocessbyorder($id,$fecharequerida,$hrrequerida,$operador,$fechaestimada,$horaestimada,$ajuste){
	$datos=array();
		$result=$this->app->getrecord("rel_procesosorden","*"," where id=$id");
		$idproceso;
		$idorden;
		$idmaquina;

		if (sizeof($result) > 0) {
			$vec2=$result[0];
			$idproceso=$vec2['idProceso'];
			$idorden=$vec2['idOrden'];
			$result2=$this->app->getrecord("procesos","*"," where id=$idproceso");
			$vec3=$result2[0];
			$idmaquina=$vec3['maquina'];

				
				$result4=$this->app->getrecord("citas","*"," where idMaquina=$idmaquina and idOrden=$idorden and idProceso=$idproceso");
		
				if (sizeof($result4) > 0) {
					$vec5=$result4[0];
					$idcita=$vec5['id'];

					$resultt=$this->app->getrecord("citas","*"," where ('$fechaestimada $horaestimada' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idMaquina=$idmaquina and citas.id!=$idcita) or ('$fechaestimada $horaestimada' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idProceso=$idproceso and citas.id!=$idcita) or ('$fechaestimada $horaestimada' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idOperador=$operador and citas.id!=$idcita) or ('$fecharequerida $hrrequerida' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idMaquina=$idmaquina and citas.id!=$idcita) or ('$fecharequerida $hrrequerida' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idProceso=$idproceso and citas.id!=$idcita) or ('$fecharequerida $hrrequerida' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idOperador=$operador and citas.id!=$idcita) ");
		
					if (sizeof($resultt) > 0) {
						$vecc=$resultt[0];
						$datos[]=array(
							'exito'=>'ya',
							'folio'=>0,
							'inicio'=>$vecc['fechaCita'].' '.$vecc['horaInicio'],
							'termino'=>$vecc['fechaFin'].' '.$vecc['horaFin']
						);
					//return $datos;
					}else{
						$this->app->updaterecord("citas"," fechaCita='$fecharequerida',horaInicio='$hrrequerida',fechaFin='$fechaestimada',horaFin='$horaestimada'","where id=$idcita");	
						if($this->app->updaterecord("rel_procesosorden"," tiempoAjuste=$ajuste,fechafinEstimada='$fechaestimada',hrfinEstimada='$horaestimada',fechaRequerida='$fecharequerida',horaRequerida='$hrrequerida',idOperador=$operador","where id=$id")){
							$datos[]=array(
								'exito'=>'si',
								'folio'=>$id
							);		
						}
					}
				}else{	
					
					$resultt=$this->app->getrecord("citas","*"," where ('$fechaestimada $horaestimada' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idMaquina=$idmaquina) or ('$fechaestimada $horaestimada' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idProceso=$idproceso) or ('$fechaestimada $horaestimada' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idOperador=$operador ) or ('$fecharequerida $hrrequerida' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idMaquina=$idmaquina) or ('$fecharequerida $hrrequerida' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idProceso=$idproceso) or ('$fecharequerida $hrrequerida' BETWEEN concat(citas.fechaCita,' ',horaInicio) AND concat(citas.fechaFin,' ',citas.horaFin) and idOperador=$operador ) ");
		
					if (sizeof($resultt) > 0) {
						$vecc=$resultt[0];
						$datos[]=array(
							'exito'=>'ya',
							'folio'=>0,
							'inicio'=>$vecc['fechaCita'].' '.$vecc['horaInicio'],
							'termino'=>$vecc['fechaFin'].' '.$vecc['horaFin']
						);
					//return $datos;
					}else{
						$this->app->saverecord("citas","null,$idmaquina,$idorden,$idproceso,$operador,5,'$fecharequerida','$hrrequerida','$fechaestimada','$horaestimada','','1'");				
						
						if($this->app->updaterecord("rel_procesosorden"," tiempoAjuste=$ajuste,fechafinEstimada='$fechaestimada',hrfinEstimada='$horaestimada',fechaRequerida='$fecharequerida',horaRequerida='$hrrequerida',idOperador=$operador","where id=$id")){
							$datos[]=array(
								'exito'=>'si',
								'folio'=>$id
							);		
						}
				
					}
					
				}


	
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}

	return $datos;	
	}
	public function updateprocessbyorderoperator ($id,$cantidadutil,$merma,$fechainicio,$hrinicio,$fechafin,$hrfin){
	$datos=array();
		$result=$this->app->getrecord("rel_procesosorden","*"," where id=$id");
		//trigger_error(print_r($result,true));
		
		if (sizeof($result) > 0) {
		
			if($this->app->updaterecord("rel_procesosorden"," cantidadUtil=$cantidadutil,merma=$merma,fechaInicio='$fechainicio',horaInicio='$hrinicio',fechaTermino='$fechafin',horaTermino='$hrfin'","where id=$id")){
					$datos[]=array(
						'exito'=>'si',
						'folio'=>$id
					);		
			}		
				
		} 
		else{
			
			$datos[]=array(
					'exito'=>'sin',
					'folio'=>0
				);
		}

	return $datos;	


	}
	public function getplaneacion($fechainicio,$fechafin,$maquina){
	$datos=array();
		$result=$this->app->getrecord("citas"," citas.id,citas.idOrden,citas.fechaCita,citas.horaInicio,citas.fechaFin,citas.horaFin,procesos.proceso"," INNER JOIN procesos on procesos.id=citas.idProceso where citas.activo like '1' and fechaCita BETWEEN '$fechainicio' and '$fechafin'  and citas.idMaquina=$maquina order by citas.fechaCita ASC ");
		$result2=$this->app->getrecord("citas"," MIN(citas.fechaCita)AS 'min', MAX(citas.fechaFin) as 'max'","where citas.activo like '1' and fechaCita BETWEEN '$fechainicio' and '$fechafin'  and citas.idMaquina=$maquina");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$vec2=$result2[0];
			$datos[] = array(
				'id'=>$vec['id'],
				'orden'=>$vec['idOrden'],
				'fechacita'=>$vec['fechaCita'],
				'horainicio'=>$vec['horaInicio'],
				'fechaFin'=>$vec['fechaFin'],
				'horaFin'=>$vec['horaFin'],
				'min'=>$vec2['min'],
				'max'=>$vec2['max'],
				'proceso'=>$vec['proceso']
				);
                }


	return $datos;

	}
	public function getmaquinasencitas($fechainicio,$fechafin){
		$datos=array();
		
		$result=$this->app->getrecord("citas"," DISTINCT(maquinas.nombre) as 'maquina',citas.idMaquina ","INNER JOIN maquinas on maquinas.id=citas.idMaquina where citas.activo like '1' and fechaCita BETWEEN '$fechainicio' and '$fechafin'");
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$datos[] = array(
				'id'=>$vec['idMaquina'],
				'maquina'=>$vec['maquina']
				);
                }


	return $datos;

	}
	public function getplaneacion2($fechainicio,$fechafin){
	$datos=array();
		$result=$this->app->getrecord("citas"," citas.id,citas.idMaquina,citas.idOrden,citas.fechaCita,citas.horaInicio,citas.fechaFin,citas.horaFin,procesos.proceso"," INNER JOIN procesos on procesos.id=citas.idProceso where citas.activo like '1' and fechaCita BETWEEN '$fechainicio' and '$fechafin' order by citas.fechaCita ASC ");
		$result2=$this->app->getrecord("citas"," MIN(citas.fechaCita)AS 'min', MAX(citas.fechaFin) as 'max'","where citas.activo like '1' and fechaCita BETWEEN '$fechainicio' and '$fechafin'");
		
		for($a=0;$a<sizeof($result);$a++)
    		{
			$vec=$result[$a];
			$vec2=$result2[0];
			
			$datos[] = array(
				'id'=>$vec['id'],
				'orden'=>$vec['idOrden'],
				'fechacita'=>$vec['fechaCita'],
				'horainicio'=>$vec['horaInicio'],
				'fechaFin'=>$vec['fechaFin'],
				'horaFin'=>$vec['horaFin'],
				'min'=>$vec2['min'],
				'max'=>$vec2['max'],
				'maquina'=>$vec['idMaquina'],
				'maquinas'=>$this->getmaquinasencitas($fechainicio,$fechafin),
				'proceso'=>$vec['proceso']
				);
                }


	return $datos;

	}

}
?>