<?php
require_once("usersFunction.php");
$app = new usersFunction();
	if(!$app->islogged()){
		echo "<script>window.top.location.href = 'logout.php';</script>";	
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="initial-scale=1.0,user-scalable=yes"/>

<link rel="stylesheet" href="../css/estilo2.css">
<link rel="stylesheet" href="../css/jquery.dataTables.min.css">
 <script language="javascript" type="text/javascript" src="../js/gateway.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery-1.12.4.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
  <script language="javascript" type="text/javascript" src="../js/solicitudes.js"></script>
<title></title>

</head>
<body onload="getClientes();">
<main>
	
	<section id="titulo">
		<center><h2> Lista de Cotizaciones</h2>
		</center>
	</section>

<div>
	<form name="f1" action="#">
		<center>
		<div id="sin2" style="display:none;">
			<div id="myDiv">
				<div class="txt">
					Ingresa los Datos de la Cotizaci�n a Buscar<br>
					<input type="text" name="num" placeholder="Cotizaci�n" id="txtbuscar" onkeyup="doSearch()" required="required" class="TT" />
				
				</div>
				<div class="txt">
						<input type="button" name="insertar" Value="Actualizar" onClick="getquotations();" id="ok"/>
				</div>
			<div >
				<img src="../img/load.gif" class="imgload" id="imgload">
			</div>
		</div>
		</center>
	</form>
</div>

<center>
</br>
	<div class="txt">
			<input type="button" name="insertar" Value="Agregar" onClick="agregar();" id="ok"/>
	</div>
	</br>
<div id="scro">
 <div id="tabla">
     <table class="tbl-qa display nowrap" style="width:100%" id="resultado">
		  <thead>
		 <tr>
		<th class="table-header" >ID</th>
		<th class="table-header" >Cliente</th>
                <th class="table-header" >Producto</th>
		<!--th class="table-header" >Tipo</th-->
	        <th class="table-header" >Fecha Registro</th>
		<th class="table-header" >Fecha Cotizacion</th>
		<th class="table-header" >Pedido SAE</th>
		<th class="table-header" >Fecha Pedido</th>
	        <th class="table-header" >Usuario</th>
		<th class="table-header" >Responsable</th>
	        <th class="table-header" >Archivos</th>
	        <th class="table-header" >Exportar</th>
	        <th class="table-header" >Estado</th>
		<th class="table-header" >Editar</th>
		<th class="table-header" >Eliminar</th>
		
              </tr>
 		  </thead>
		  <tbody style="height:250px;overflow:scroll">				
             	 </tbody>
              </table>
 </div>
	</br></br>
		<div id="sin" style="display:none;">
			<div class='myDiv'><div class='txt'>NO HAY COTIZACIONES REGISTRADAS EN EL SISTEMA </div></div>
		</div>
 </div>
</center>
</div>


<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content"> 
  <div class="modal-content2">	
    <span class="close">&times;</span>


			<section id="titulo">
        			<center></br><h2>Ingrese los datos de la nueva cotizaci�n</h2>
					     <h5>Asegurese de que los datos introducidos sean correctos</h5>
				</center>
			</section>
<div class="contenedor">
			
	<div class="myDiv" >
		
		<div class="txt" >
			Cliente<br>
			<select id="optioncliente"></select>	
		</div>
	</div>
</div>

<div id="productos">
		<section id="titulo">
			<center><h2>Productos</h2>
			</center>
		</section>	
		
	<div class="contenedor">
		<div class="myDiv" >
			<div class="txt" >
				Tipo de producto<br>
				<select id="optionproducto"></select>	
			</div>
			<div class="txt" >
				C�digo de Identificaci�n �nico<br>
				<input type="text" placeholder="Id �nico" id="txtidunico" class="TT"/>		
			</div>
		</div>
	</div>
		<section id="titulo">
			<center><h2>Descriptivo Conceptual y Dimensional</h2><h3>Dimensiones del producto en mm</h3>
			</center>
		</section>
		
	<div class="contenedor">
		<div class="myDiv" >
			<div class="txt" >
				A<br>
				<input type="number" placeholder="Ancho" id="txta" class="TT"/>		
			</div>

			<div class="txt" >
				B<br>
				<input type="number" placeholder="Largo" id="txtb" class="TT"/>		
			</div>
			<div class="txt" >
				H<br>
				<input type="number" placeholder="Alto" id="txth" class="TT"/>		
			</div>
			<div class="txt" >
				X<br>
				<input type="number" placeholder="Ancho" id="txtx" class="TT"/>		
			</div>

			<div class="txt" >
				Y<br>
				<input type="number" placeholder="Largo" id="txty" class="TT"/>		
			</div>
			<div class="txt" >
				Z<br>
				<input type="number" placeholder="Alto" id="txtz" class="TT"/>		
			</div>
			<div class="txt" >
				O<br>
				<input type="number" placeholder="Ancho" id="txto" class="TT"/>		
			</div>

			<div class="txt" >
				P<br>
				<input type="number" placeholder="Largo" id="txtp" class="TT"/>		
			</div>
			<div class="txt" >
				Q<br>
				<input type="number" placeholder="Alto" id="txtq" class="TT"/>		
			</div>
			<div class="txt" >
				R<br>
				<input type="number" placeholder="Ancho" id="txtr" class="TT"/>		
			</div>

			<div class="txt" >
				S<br>
				<input type="number" placeholder="Largo" id="txts" class="TT"/>		
			</div>
			<div class="txt" >
				T<br>
				<input type="number" placeholder="Alto" id="txtt" class="TT"/>		
			</div>
			<div class="txt" >
				C�digo<br>
				<select id="optioncodigo"></select>	
			</div>
			<div class="txt" >
				Descriptivo de Variables a Considerar<br>
				<textarea rows="3" cols="30" name="dir" id="txtnotas" placeholder="..."></textarea>		
			</div>
				
		</div>
	</div>

	<section id="titulo">
			<center><h2>Descriptivo de Materiales y Acabados</h2>
			</center>
		</section>
		
	<div class="contenedor">
		<div class="myDiv" >
			<div class="txt" >
				Categorias<br>
				<select id="optioncategorias" onChange="getsustratosss(this.value);"></select>	
			</div>
			<div class="txt" >
				Sustrato Base<br>
				<select id="optiontiposustrato"></select>	
			</div>
			<div class="txt" >
				Empalme<br>
				<select id="optionempalme"></select>	
			</div>
			<div class="txt" >
				Suaje<br>
				<select id="optionsuaje"></select>	
			</div>
			<div class="txt" >
				Pegado<br>
				<select id="optionpegado"></select>	
			</div>
		</div>
	</div>

		<section id="titulo">
			<center><h2>Impresi�n y Acabados</h2>
			</center>
		</section>
		
	<div class="contenedor">
		<div class="myDiv" >
			<div class="txt" >
				Tipo de tinta Frente<br>
				<input type="number" placeholder="Frente" id="txttintafrente" class="TT"/>
				<select id="optiontintafrente" ></select>	
			</div>
			<div class="txt" >
				Tipo de tinta Reverso<br>
				<input type="number" placeholder="Frente" id="txttintareverso" class="TT"/>
				<select id="optiontintareverso" ></select>	
			</div>
			<div class="txt" >
				Pantones Frente<br>
				<input type="number" placeholder="Pantones" id="txtnumeropantonesfrente" class="TT"/>
				<input type="text" placeholder="Pantones" id="txtpantonesfrente" class="TT"/>		
			</div>
			<div class="txt" >
				Pantones Reverso<br>
				<input type="number" placeholder="Pantones" id="txtnumeropantonesreverso" class="TT"/>
				<input type="text" placeholder="Pantones" id="txtpantonesreverso" class="TT"/>		
			</div>
			<div class="txt" >
				Barniz Frente<br>
				<select id="optionbarnizfrente"></select>	
			</div>
			<div class="txt" >
				Barniz Reverso<br>
				<select id="optionbarnizreverso"></select>	
			</div>
			<div class="txt" >
				Laminado Frente<br>
				<select id="optionlaminadofrente"></select>	
			</div>
			<div class="txt" >
				Laminado Reverso<br>
				<select id="optionlaminadoreverso"></select>	
			</div>
			<div class="txt" >
				Serigraf�a Frente<br>
				<select id="optionserigrafiafrente"></select>	
			</div>
			<div class="txt" >
				Serigraf�a Reverso<br>
				<select id="optionserigrafiareverso"></select>	
			</div>
			<div class="txt" >
				Cambios de Arte Frente<br>
				<input type="number" placeholder="Camios de arte" id="txtcambiosartefrente" class="TT"/>		
			</div>
			<div class="txt" >
				Cambios de Arte Reverso<br>
				<input type="number" placeholder="Camios de arte" id="txtcambiosartefrente" class="TT"/>		
			</div>
		</div>
	</div>
			<section id="titulo">
			<center><h2>Ventanas</h2>
			</center>
		</section>
		
	<div class="contenedor">
		<div class="myDiv" >
			<div class="txt" >
				Material<br>
				<select id="optiontiposustrato"></select>	
			</div>
			<div class="txt" >
				Largo<br>
				<input type="number" placeholder="Ventana Largo (mm)" id="txtventanaalargo" class="TT"/>		
			</div>
			<div class="txt" >
				Alto<br>
				<input type="number" placeholder="Ventana Alto (mm)" id="txtventanaaalto" class="TT"/>		
			</div>
		</div>
	</div>


		<section id="titulo">
			<center><h2>Terminos y Condiciones Comerciales</h2>
		</center>
		</section>	
		</br>
	<div class="contenedor">
		<div class="myDiv">
			<div class="txt" >
				Separar Herramental<br>
				<select id="optionherramental"></select>	
			</div>
			<div class="txt" >
				Separar Cambios de Arte<br>
				<select id="optioncambiosarte"></select>	
			</div>
			<div class="txt" >
				Requiere Dummy<br>
				<select id="optiondummy"></select>	
			</div>
			<div class="txt" >
				Agregar a Web to Print<br>
				<select id="optiondummy"></select>	
			</div>
		</div>


	</div>

		<section id="titulo">
			<center><h2>Escala de Volumen Solicitada</h2>
			</center>
		</section>
	<div class="contenedor">
		<div class="myDiv">
			<div class="txt" >
				Primera escala<br>
				<select id="optionprimeraescala"></select>	
			</div>
			<div class="txt" >
				Segunda escala<br>
				<select id="optionsegundaescala"></select>	
			</div>
			<div class="txt" >
				Tercera escala<br>
				<select id="optionterceraescala"></select>	
			</div>
			<div class="txt" >
				Cuarta escala<br>
				<select id="optioncuartaescala"></select>	
			</div>
			<div class="txt" >
				Quinta escala<br>
				<select id="optionquintaescala"></select>	
			</div>
			<div class="txt" >
				Sexta escala<br>
				<select id="optionsextaescala"></select>	
			</div>
			<div class="txt" >
				Costo Objetivo<br>
				<input type="number" placeholder="Costo Objetivo" id="txtcostoobjetivo" class="TT"/>		
			</div>
			<div class="txt" >
				Margen de Ganancia<br>
				<select id="optionmargenganacia"></select>	
			</div>
		</div>


	</div>
		<section id="titulo">
			<center><h2>Terminos Generales</h2>
		</center>
		</section>	
		</br>
	<div class="contenedor">
		<div class="myDiv">
			<div class="txt" >
				Vigencia de la cotizaci�n<br>
				<select id="optionvigenciacotizacion"></select>	
			</div>
			<div class="txt" >
				Forma de pago<br>
				<select id="optionformapago"></select>	
			</div>
			<div class="txt" >
				Flete<br>
				<select id="optionformaentrega"></select>	
			</div>	
			
		</div>


	</div>
	<section id="titulo">
			<center><h2>Especificaciones del Embalaje</h2>
		</center>
		</section>	
		</br>
	<div class="contenedor">
		<div class="myDiv">
			<div class="txt" >
				Inner Pack<br>
				<input type="number" placeholder="Inner Pack" id="txtmultiploinner" class="TT"/>
				<select id="optioninner"></select>	
			</div>
			<div class="txt" >
				Master Pack<br>
				<input type="number" placeholder="Master Pack" id="txtnultiplomaster" class="TT"/>
				<select id="optionmaster"></select>	
			</div>	
			
		</div>


	</div>
	<center>
		<div class="txt">
			<input type="button" name="insertar" Value="Guardar" onClick="savecotizacionproducto();" id="ok"/>
		</div>
	</center>

</div>

</div>
</div>
  
</div>
	



<!--/////////////////////////////////////////////////////view files of product//////////////////////////////////////////////////////////////////////////////-->


<div id="myModalfiles" class="modal">

  <!-- Modal content -->
  <div class="modal-content"> 
  <div class="modal-content2">	
    <span class="close" id="closefiles">&times;</span>
			<section id="titulo">
        			<center></br><h2>Archivos de la cotizaci�n</h2>
					     </br><h3 id="filescotizacion"></h3>
				</center>
			</section>
			</br>
	
	<center>
	</br>
	

		<div class="txt">

			<img src="../img/folder.png" onClick="addfile2();"/>
			
		</div>
		<div class="myDiv" id="docs22" style="display:none;">
	
			<div class="txt">
				Seleccione el archivo<br>
		    		<input type="file" id="file12" class="TT" onchange="enablebtn2();"/>
             	   		
				<center>
					<!--input type="button" name="insertar" Value="Guardar" onClick="" id="ok"/-->
					<textarea rows="6" cols="50" name="dir"  class="TT" id="txtfiledescription2" placeholder="Descripci�n del archivo"></textarea>
					<br>						
					<img src="../img/save2.png" onclick="uploadfiletoserver2();" id="imgsavefile2" style="display:none;"/>
				</center>
			</div>		
		</div>
		<div >
			<img src="../img/load.gif" class="imgload" id="imgloadfiles">
		</div>
	<div id="scro">

 		<div id="tablafiles">
   			  <table class="tbl-qa" id="resultadofiles">
		 		 <thead>
					 <tr>
						<th class="table-header" >Descripcion</th>
						<th class="table-header" >Ver</th>
						<th class="table-header" >Editar</th>
						<th class="table-header" >Eliminar</th>
						
              				</tr>
 		  		</thead>
		  		<tbody >				
             	 		</tbody>
              		</table>
 		</div>
		</br></br>
		<div id="sinfiles" style="display:none;">
			<div class='myDiv'><div class='txt'>NO HAY ARCHIVOS REGISTRADOS EN EL SISTEMA </div></div>
		</div>
 	</div>
	</center>	



  </div>
  </div>
  
</div>



</main>
</body>
</html>
