-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 22-05-2018 a las 12:43:42
-- Versión del servidor: 5.7.17-log
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `planeacioncontrol`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `idPersona` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `razonSocial` varchar(255) NOT NULL,
  `domicilioFiscal` varchar(500) NOT NULL,
  `giro` varchar(200) NOT NULL,
  `exporta` enum('0','1') NOT NULL,
  `etiqueta` enum('0','1') NOT NULL,
  `carton` enum('0','1') NOT NULL,
  `corrugado` enum('0','1') NOT NULL,
  `caja` enum('0','1') NOT NULL,
  `display` enum('0','1') NOT NULL,
  `tiraje` enum('0','1') NOT NULL,
  `datoVariable` enum('0','1') NOT NULL,
  `metalizado` enum('0','1') NOT NULL,
  `disenio` enum('0','1') NOT NULL,
  `web` enum('0','1') NOT NULL,
  `ventas` enum('0','1') NOT NULL,
  `costosDirectos` enum('0','1') NOT NULL,
  `costosnoEvidentes` enum('0','1') NOT NULL,
  `participacion` enum('0','1') NOT NULL,
  `satisfaccionCliente` enum('0','1') NOT NULL,
  `productos` varchar(500) NOT NULL,
  `consumidor` varchar(500) NOT NULL,
  `canal` varchar(500) NOT NULL,
  `competencia` varchar(500) NOT NULL,
  `vision` varchar(500) NOT NULL,
  `estimacionVentasanual` varchar(255) NOT NULL,
  `necesidades` varchar(500) NOT NULL,
  `problematica` varchar(500) NOT NULL,
  `proveedores` varchar(500) NOT NULL,
  `expectativas` varchar(500) NOT NULL,
  `activo` enum('0','1') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `idPersona`, `idUsuario`, `razonSocial`, `domicilioFiscal`, `giro`, `exporta`, `etiqueta`, `carton`, `corrugado`, `caja`, `display`, `tiraje`, `datoVariable`, `metalizado`, `disenio`, `web`, `ventas`, `costosDirectos`, `costosnoEvidentes`, `participacion`, `satisfaccionCliente`, `productos`, `consumidor`, `canal`, `competencia`, `vision`, `estimacionVentasanual`, `necesidades`, `problematica`, `proveedores`, `expectativas`, `activo`) VALUES
(77, 33, 12, 'Patito S.A. de C.V.', 'Domicilio conocido Villa Victoria', 'Automotriz', '0', '0', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', 'Productos AEG2', 'Consumidor AEG2', 'Canal de venta AEG2', 'Competencia AEG2', 'Visión AEG2', 'Estimacion AEG2', 'Necesidades AEG2', 'Problematica AEG2', 'Provedores AEG2', 'Espectativas AEG2', '1'),
(78, 34, 19, 'otra', 'pemsa', 'Cerraduras y Candados.', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', '', '1');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
